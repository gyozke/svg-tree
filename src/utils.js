/** @license MIT License (c) copyright 2016 */
/** @author Fejes Bence */

/**
 * Flattens a VDAG into a forest. Basically it creates new
 * nodes for each virtual connection.
 *
 * @param roots :: [VDAGNode<T>]
 * @returns [Node<T>]
 */
export function convertVDAGToForest(roots) {
    const go = (current, virtual, child) => ({
        id: virtual ? current.id + '_' + child.id : String(current.id),
        parents: virtual ? [] : current.parents.map(({node, virtual}) => go(node, virtual, current)),
        data: {...current.data, id: current.id, virtual}
    })

    return roots.map(root => go(root, false))
}

/**
 * Returns the nodes of the forest with pre-order traversal.
 *
 * @param forest :: [Node<T>]
 * @returns [Node<T>]
 */
export function forestPreorderTraverse(forest) {
    const go = (node) => [node].concat(flatMap(node.parents, go))

    return flatMap(forest, go)
}

/**
 * Returns the nodes of the forest with post-order traversal.
 *
 * @param forest :: [Node<T>]
 * @returns [Node<T>]
 */
export function forestPostorderTraverse(forest) {
    const go = (node) => flatMap(node.parents, go).concat(node)

    return flatMap(forest, go)
}

/**
 * Flattens a VDAG into a DAG. Basically it just drops the
 * `virtual` properties from the `parents`.
 *
 * @param roots :: [VDAGNode<T>]
 * @returns [Node<T>]
 */
export function convertVDAGToDAG(roots) {
    const go = (current) => ({
        ...current,
        parents: current.parents.map(({node}) => go(node))
    })

    return roots.map(go)
}

/**
 * Calculates the dimensions of the bounding boxes for
 * each node in the forest.
 *
 * @param forest :: [Node<T>]
 * @param config :: Object
 * @returns [Node<T>]
 */
function calculateBoundingBoxForForest(forest, config) {
    const go = (node) => {
        const nodeHeight = config.USE_DEFAULT_SIZE ? config.DEFAULT_NODE_HEIGHT : node.data.height || config.DEFAULT_NODE_HEIGHT
        const nodeWidth = config.USE_DEFAULT_SIZE ? config.DEFAULT_NODE_WIDTH : node.data.width || config.DEFAULT_NODE_WIDTH

        const parents = node.parents.map(go)
        const parentDimensions = parents
            .reduce(({height, width}, parent) => ({
                height: height + parent.data.boxHeight + config.VERTICAL_GAP,
                width: Math.max(width, parent.data.boxWidth + config.HORIZONTAL_GAP)

            }), {
                height: 0,
                width: 0
            })

        const boxHeight = Math.max(parentDimensions.height - (parents.length > 0 ? config.VERTICAL_GAP : 0), nodeHeight)
        const boxWidth = parentDimensions.width + nodeWidth

        return {
            ...node,
            parents,
            data: {
                ...node.data,
                height: nodeHeight,
                width: nodeWidth,
                boxHeight,
                boxWidth
            }
        }
    }

    return forest.map(go)
}

/**
 * Calculates the position of each node from the top left corner
 * of the canvas. The trees are built from left to right, starting
 * with the leaves.
 *
 * @param forest :: [Node<T>]
 * @param config :: Object
 * @returns [Node<T>]
 */
export function calculateOffsetsForForest(forest, config) {
    const go = (node, treeTopOffset, childLeftOffset, treeTotalWidth) => {
        const {parents} = node.parents
            .reduce((acc, parent) => ({
                topOffset: acc.topOffset + parent.data.boxHeight + config.VERTICAL_GAP,
                parents: acc.parents
                    .concat(go(parent, acc.topOffset, childLeftOffset + node.data.width + config.HORIZONTAL_GAP, treeTotalWidth))

            }), {
                topOffset: treeTopOffset,
                parents: []
            })

        return {
            ...node,
            parents,
            data: {
                ...node.data,
                topOffset: treeTopOffset + node.data.boxHeight / 2,
                leftOffset: treeTotalWidth - childLeftOffset - node.data.width / 2
            }
        }
    }

    const {parents} = calculateBoundingBoxForForest(forest, config)
        .reduce((acc, node) => {
            const parent = go(node, acc.topOffset, 0, node.data.boxWidth)

            return {
                topOffset: acc.topOffset + parent.data.boxHeight + config.VERTICAL_GAP,
                parents: acc.parents.concat(parent)
            }
        }, {
            topOffset: 0,
            parents: []
        })

    return parents
}

/**
 * Removes one level depth from the array.
 *
 * @param array :: [[T]]
 * @returns [T]
 */
const flatten = (array) => array.reduce((a, b) => a.concat(b), [])


/**
 * Maps a function on the array elements, then concatenates the result.
 *
 * @param array :: [T]
 * @param project :: T -> [R]
 * @returns [R]
 */
const flatMap = (array, project) => flatten(array.map(project))


export function mergeOpenedInformationIntoForest(forest, openedStageIds) {
    const go = node => {
      
      var plusdata = {};
      if (openedStageIds.includes(node.id)) {
        plusdata = {
          opened: true,
          width: 300,
          height: 500,
        }
      }
      
      return {
        ...node,
        parents: node.parents.map(go),
        data: {
            ...node.data,
            ...plusdata,
        }
      };
      
    }
    
    return forest.map(go);
}