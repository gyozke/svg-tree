import React from 'react';
import ReactDom from 'react-dom';
import Application from './components/application';
import Header from './components/header';

import getForest from './forestmock.2.js';

ReactDom.render(
  // <Header text={'alma'} />,
  <Application forest={getForest()} />,
  document.getElementById('container')
);
