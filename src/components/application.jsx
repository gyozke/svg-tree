import React from 'react';
import SVGCanvas from './svgcanvas';
import SVGStageForest from './svgstageforest';
import SVGDetails from './svgdetails';
import SVGCircle from './svgcircle';
import SVGInkscape1 from './svginkscape1';
import SVGNode from './svgnode';

export default class Application extends React.Component {
  render() {
    return (
      <div>
        <SVGCanvas>
          <SVGDetails top="100" left="200" />
          <SVGCircle top="100" left="500" />
          <SVGInkscape1 top="100" left="700" />
            <SVGNode top="100" left="1100"/>
        </SVGCanvas>
        <SVGCanvas>
          <SVGStageForest forest={this.props.forest} />
        </SVGCanvas>
      </div>
    );   
  }
}
