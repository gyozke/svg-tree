import React from 'react';

export default class SVGGroup extends React.Component {
  render() {
    const translateX = this.props.leftOffset || 0;
    const translateY = this.props.topOffset || 0;
    const style = {
      transform: `translate(${translateX}px, ${translateY}px)`,
    }
    
    return (
      <g style={style}>
        {this.props.children}
      </g>      
    );
  }
}



