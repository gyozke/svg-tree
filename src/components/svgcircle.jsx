import React from 'react';
import SVGGroup from './svggroup';

export default class SVGCircle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      details: false,
    }
  }
  
  setDetails(value) {
    this.setState({
      details: value,
    });
  }
  
  onMouseOver(e) {
    this.setDetails(true);
  }
  
  onMouseOut(e) {
    this.setDetails(false);
  }
  
  render() {
    
    const details = this.state.details;
    
    return (
      <SVGGroup leftOffset={this.props.left} topOffset={this.props.top}>
        <circle cx="60" cy="60" r="50" 
          onMouseOver={(e) => this.onMouseOver(e)}
          onMouseOut={(e) => this.onMouseOut(e)}
        />
        { 
          details ?
            <g transform="translate(100,0)">
              <rect x="10" y="10" width="100" height="10" fill="white" />
            </g>
            : null   
        }
        
      </SVGGroup>
    );
  }
}
