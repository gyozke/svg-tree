import React from 'react';
import SVGGroup from './svggroup';

export default class SVGDetails extends React.Component {
  render() {
    return (
      <SVGGroup leftOffset={this.props.left} topOffset={this.props.top}>
        <svg width="270" height="254">
          <rect x="1%" y="1%" rx="5%" ry="5%" width="95%" height="95%" fill="none" stroke="black" stroke-width="1"/>
          <g transform="translate(4,4)">

            <g transform="translate(10,10)">
              <rect width="235" height="30" fill="none" stroke="black" stroke-width="0.5"/>
              <g>
                <rect width="20" height="32" fill="lightgreen"/>
                <text x="7" y="12" style={{fontSize:'9px',fontFamily:'Verdana'}} transform="rotate(270 11,11)">COMP</text>
              </g>
              <g transform="translate(125,14)">
                <text alignment-baseline="baseline" style={{fontSize:'9px',fontFamily:'Verdana'}}>#128 <tspan font-weight="bold">☼</tspan> <tspan fill="green">12.13</tspan>/<tspan fill="red">11.12</tspan>/<tspan fill="blue">22.32</tspan></text>
                <text y="10" style={{fontSize:'9px',fontFamily:'Verdana'}}><tspan font-weight="bold" font-size="12">&#8594;</tspan> <tspan fill="green">12.13</tspan>/<tspan fill="red">11.12</tspan>/<tspan fill="blue">22.32</tspan> <tspan font-weight="bold">$</tspan> <tspan fill="green">12.13</tspan>/<tspan fill="red">11.12</tspan>/<tspan fill="blue">22.32</tspan></text>
              </g>
            </g>

            <g transform="translate(10,45)">
              <rect width="235" height="30" fill="none" stroke="black" stroke-width="0.5"/>
              <g>
                <rect width="20" height="30" fill="red"/>
                <text x="7" y="12" style={{fontSize:'9px',fontFamily:'Verdana'}} transform="rotate(270 11,11)">COMP</text>
              </g>
              <g transform="translate(125,14)">
                <text style={{fontSize:'9px',fontFamily:'Verdana'}}>#128 <tspan font-weight="bold">☼</tspan> <tspan fill="green">12.13</tspan>/<tspan fill="red">11.12</tspan>/<tspan fill="blue">22.32</tspan></text>
                <text y="10" style={{fontSize:'9px',fontFamily:'Verdana'}}><tspan font-weight="bold" font-size="12">&#8594;</tspan> <tspan fill="green">12.13</tspan>/<tspan fill="red">11.12</tspan>/<tspan fill="blue">22.32</tspan> <tspan font-weight="bold">$</tspan> <tspan fill="green">12.13</tspan>/<tspan fill="red">11.12</tspan>/<tspan fill="blue">22.32</tspan></text>
              </g>
            </g>

            <g transform="translate(10,80)">
              <rect width="235" height="30" fill="none" stroke="black" stroke-width="0.5"/>
              <g>
                <rect width="20" height="30" fill="lightgreen"/>
                <text x="7" y="12" style={{fontSize:'9px',fontFamily:'Verdana'}} transform="rotate(270 11,11)">INPR</text>
              </g>
              <g transform="translate(125,14)">
                <text style={{fontSize:'9px',fontFamily:'Verdana'}}>#128 <tspan font-weight="bold">☼</tspan> <tspan fill="green">12.13</tspan>/<tspan fill="red">11.12</tspan>/<tspan fill="blue">22.32</tspan></text>
                <text y="10" style={{fontSize:'9px',fontFamily:'Verdana'}}><tspan font-weight="bold" font-size="12">&#8594;</tspan> <tspan fill="green">12.13</tspan>/<tspan fill="red">11.12</tspan>/<tspan fill="blue">22.32</tspan> <tspan font-weight="bold">$</tspan> <tspan fill="green">12.13</tspan>/<tspan fill="red">11.12</tspan>/<tspan fill="blue">22.32</tspan></text>
              </g>
            </g>

            <g transform="translate(10,115)">
              <rect width="235" height="30" fill="none" stroke="black" stroke-width="0.5"/>
              <g>
                <rect width="20" height="30" fill="red"/>
                <text x="7" y="12" style={{fontSize:'9px',fontFamily:'Verdana'}} transform="rotate(270 11,11)">INPR</text>
              </g>
              <g transform="translate(125,14)">
                <text style={{fontSize:'9px',fontFamily:'Verdana'}}>#128 <tspan font-weight="bold">☼</tspan> <tspan fill="green">12.13</tspan>/<tspan fill="red">11.12</tspan>/<tspan fill="blue">22.32</tspan></text>
                <text y="10" style={{fontSize:'9px',fontFamily:'Verdana'}}><tspan font-weight="bold" font-size="12">&#8594;</tspan> <tspan fill="green">12.13</tspan>/<tspan fill="red">11.12</tspan>/<tspan fill="blue">22.32</tspan> <tspan font-weight="bold">$</tspan> <tspan fill="green">12.13</tspan>/<tspan fill="red">11.12</tspan>/<tspan fill="blue">22.32</tspan></text>
              </g>
            </g>
            <g transform="translate(10,150)">

              <g>
                <rect height="20" fill="none" stroke="black" stroke-width="1" width="235"/>
                <text x="75" y="15" style={{fontSize:'12px'}}>☼ .../.../... &#8594; .../.../... $ .../.../...</text>
              </g>

              <g transform="translate(0,25)">
                <rect width="235" height="20" fill="none" stroke="black" stroke-width="1"/>
                <text x="75" y="15" style={{fontSize:'12px'}}>☼ .../.../... &#8594; .../.../... $ .../.../...</text>
              </g>

              <g transform="translate(0,50)">
                <rect width="235" height="20" fill="none" stroke="black" stroke-width="1"/>
                <text x="75" y="15" style={{fontSize:'12px'}}>☼ .../.../... &#8594; .../.../... $ .../.../...</text>
              </g>
            </g>
          </g>
        </svg>
      </SVGGroup>
    );
  }
}
