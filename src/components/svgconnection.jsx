import React from 'react';
import SVGGroup from './svggroup';
import config from '../config';

export default class SVGStageConnection extends React.Component {
  onClick(e) {
    // console.log(this.props.params);
    // const conn = this.props.params;
    // console.log(new Set([
    //   conn,
    //   ...conn.parents,
    // ]));
    this.props.onClick(this.props.params);
  }
  
  render() {
    const node = this.props.params;
    
    const lines = node.parents.reduce((acc, parent) => {
        const parentLeftOffset = parent.data.leftOffset - node.data.leftOffset + parent.data.width / 2;
        const parentTopOffset = parent.data.topOffset - node.data.topOffset;
        
        const nodeLeftOffset = -node.data.width / 2;
        
        const centerLeftOffset = (parentLeftOffset + nodeLeftOffset) / 2
        // const centerLeftOffset = (parentLeftOffset + nodeLeftOffset) / 2
        
        const leftControlLeftOffset = parentLeftOffset + Math.abs((parentLeftOffset - nodeLeftOffset) / 3);
        const rightControlLeftOffset = parentLeftOffset + Math.abs(2 * (parentLeftOffset - nodeLeftOffset) / 3);

        switch (config.CONNECTION_STYLE) {
            case 'line':
                return acc.concat([
                  <line 
                    key={node.id + '_' + parent.id}
                    x1="0"
                    y1="0"
                    x2={parentLeftOffset}
                    y2={parentTopOffset}
                    className="connection"
                  />,
                  <line 
                    key={node.id + '_' + parent.id + '_invisible'}
                    x1="0"
                    y1="0"
                    x2={parentLeftOffset}
                    y2={parentTopOffset}
                    className="invisible"
                  />
                ]);
            case 'curve':
                return acc.concat([
                    <path 
                      key={node.id + '_' + parent.id + '_1'}
                      d={`M${parentLeftOffset},${parentTopOffset} Q${leftControlLeftOffset},${parentTopOffset} ${centerLeftOffset},${parentTopOffset/2}`}
                      className="connection"
                    />,
                    <path 
                      key={node.id + '_' + parent.id + '_2'}
                      d={`M${centerLeftOffset},${parentTopOffset/2} Q${rightControlLeftOffset},0 ${nodeLeftOffset},0`}
                      className="connection"
                    />,
                    <path 
                      key={node.id + '_' + parent.id + '_1_invisible'}
                      d={`M${parentLeftOffset},${parentTopOffset} Q${2*parentLeftOffset/3},${parentTopOffset} ${parentLeftOffset/2},${parentTopOffset/2}`}
                      className="invisible"
                    />,
                    <path 
                      key={node.id + '_' + parent.id + '_2_invisible'}
                      d={`M${parentLeftOffset/2},${parentTopOffset/2} Q${parentLeftOffset/3},0 0,0`}
                      className="invisible"
                    />,
                ]);
        }

    }, []);
    
    
    return (
      <SVGGroup 
        leftOffset={node.data.leftOffset} 
        topOffset={node.data.topOffset}
      >
        <g onClick={(e) => this.onClick(e)}>
          {lines}
        </g>
      </SVGGroup>
    );
  }
}
