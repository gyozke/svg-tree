import React from 'react';
import SVGGroup from './svggroup';

export default class SVGNode extends React.Component {
    render() {
        let cx=50;
        let cy=50;
        let i =0;
        let total = 100;
        let angles = [];
        let startAngle = 0;
        let r = 50;
        let data = [50, 10, 40];
        //const colors = ['red', 'white', 'green'];
        const pathData = new Array();

        //zold
        angles[0] = data[0] / total * Math.PI * 2;
        //piros
        angles[1] = data[1] / total * Math.PI * 2;
        //feher
        angles[2] = data[2] / total * Math.PI * 2;
        for (i = 0; i < data.length; i++) {
            let endAngle = startAngle + angles[i];
            let x1 = cx + r * Math.sin(startAngle);
            let y1 = cy - r * Math.cos(startAngle);
            let x2 = cx + r * Math.sin(endAngle);
            let y2 = cy - r * Math.cos(endAngle);
            let big = 0;

            if (endAngle - startAngle > Math.PI) {
                big = 1;
            }
            pathData[i] = 'M ' + cx + ',' + cy + // Start at circle center
                ' L ' + x1 + ',' + y1 + // Draw line to (x1,y1)
                ' A ' + r + ',' + r + // Draw an arc of radius r
                ' 0 ' + big + ' 1 ' + // Arc details...
                x2 + ',' + y2 + // Arc goes to to (x2,y2)
                ' Z'; // Close path back to (cx,cy)
            startAngle = endAngle;
        }
        return (
            <SVGGroup leftOffset={this.props.left} topOffset={this.props.top}>
                <path d={pathData[0]} fill='red'/>
                <path d={pathData[1]} fill='white'/>
                <path d={pathData[2]} fill='green'/>
            </SVGGroup>
        );
    }
}