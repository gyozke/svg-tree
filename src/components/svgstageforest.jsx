import React from 'react';
import SVGCanvas from './svgcanvas';
import SVGRectangle from './svgrectangle';
import SVGDetails from './svgdetails';
import SVGStageConnection from './svgconnection';

import config from '../config';
import * as utils from '../utils';

export default class SVGStageForest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openedConnections: new Set(),
    };
  }
  
  onConnectionClick(conn) {
    const openedConnections = this.state.openedConnections;
    if ([...openedConnections.values()].map(conn => conn.id).includes(conn.id)) {
      for (let c of openedConnections) {
        if (c.id === conn.id) {
          openedConnections.delete(c);
          break;  
        }  
      }
    } else {
      openedConnections.add(conn);
    }
    this.setState({
      openedConnections
    });
  }
  
  getStagesForConnection(conn) {
    return [
      conn,
      ...conn.parents,
    ];
  }
  
  render() {
    const openedStages = [];
    this.state.openedConnections.forEach((conn) => {
      openedStages.push(...this.getStagesForConnection(conn).map(stage => stage.id));
    });
   
    const mergedForest = utils.mergeOpenedInformationIntoForest(this.props.forest, openedStages);
     
    const forest = utils.calculateOffsetsForForest(mergedForest, config);
    
    const nodes = utils.forestPreorderTraverse(forest)
        .sort((lhs, rhs) => {
            if (lhs.id < rhs.id) return -1
            if (rhs.id < lhs.id) return 1
            return 0
        });
    
    
    // console.log(openedStages);
    
    const stages = nodes.map(stage => {
      // console.log(stage.id, openedStages[0] === stages)
      return <SVGRectangle 
        params={stage} 
        key={stage.id} 
        opened={stage.data.opened} 
      />
    });
    const connections = nodes.map(stage => 
      <SVGStageConnection 
        params={stage} 
        key={'connection_' + stage.id}
        onClick={this.onConnectionClick.bind(this)} 
      />);
    
    return (
      <SVGCanvas>
        {connections}
        {stages}
      </SVGCanvas>
    );   
  }
}
