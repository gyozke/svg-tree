import React from 'react';
import SVGGroup from './svggroup';

export default class SVGRectangle extends React.Component {
  render() {
    const {params, opened} = this.props;
    
    return (
      <SVGGroup 
        leftOffset={params.data.leftOffset} 
        topOffset={params.data.topOffset}
      >
        <rect
          width={params.data.width}
          height={params.data.height}
          x={-params.data.width / 2}
          y={-params.data.height / 2}
          rx="10"
          ry="10"
          className={'node ' + (params.data.virtual ? 'virtual' : 'normal')} 
        />
        <text 
          transform="translate(0, 8)"
        >
          {params.id}
          {opened ? 'opened' : null}
        </text>
      </SVGGroup>
    );
  }
}






