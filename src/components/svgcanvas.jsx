import React from 'react';

export default class SVGCanvas extends React.Component {
  render() {
    return (
      <svg width="1600" height="1400">
        {this.props.children}
      </svg>
    );
  }
}