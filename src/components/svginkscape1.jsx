import React from 'react';
import SVGGroup from './svggroup';

export default class SVGInkscape1 extends React.Component {
  render() {
    return (
      <SVGGroup leftOffset={this.props.left} topOffset={this.props.top}>
        <rect rx="3" ry="3" height="600" width="300" stroke="#000" stroke-width="3" fill="none"/>
        <g transform="translate(10,10)">
          <rect height="44.86" width="280" stroke="#000" stroke-width="1" fill="none"/>
          <g>
            <rect height="45" width="15" stroke="#000" fill="#0f0" />
            <text style={{wordSpacing:'0px',letterSpacing:'0px',fontSize:'10px'}} x="-23" y="10" transform="rotate(-90)" font-size="10px" font-family="sans-serif" line-height="125%" fill="#000000"><tspan >Comp</tspan></text>
          </g>
          <text style={{wordSpacing:'0px',letterSpacing:'0px',fontSize:'10px'}} text-anchor="start" x="42" y="12" font-size="10px" font-family="sans-serif" line-height="125%" fill="#000000"><tspan>#184 ☼34</tspan></text>
        </g>    
      </SVGGroup>
    );
  }
}
