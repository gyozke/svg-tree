export default {
  "stages": [
    { "id": 1, "parents": [], "radius": 20 },
    { "id": 2, "parents": [] },
    { "id": 3, "parents": [1, 2], "radius": 50 },
    { "id": 4, "parents": [3] },
    { "id": 5, "parents": [2], "radius": 50 },
    { "id": 6, "parents": [5, 3] },
    { "id": 7, "parents": [4, 6, 1] },
    { "id": 8, "parents": [7], "radius": 50 },
    { "id": 9, "parents": [8, 7] },
    { "id": 10, "parents": [] },
    { "id": 11, "parents": [10] },
    { "id": 12, "parents": [11, 1, 2, 3] },
    { "id": 13, "parents": [8, 12] },
    { "id": 14, "parents": [8, 12], "radius": 40 },
    { "id": 15, "parents": [14] },
    { "id": 16, "parents": [10, 15], "radius": 70 },
    { "id": 17, "parents": [9, 16] },
    { "id": 18, "parents": [13, 17], "radius": 80 },
    { "id": 19, "parents": [18] }
  ]
};
