export default function getForest() {
    // csomópontok
    var node1 = { id: 1, parents: [], data: {width: 500, height: 70} }; // itt egy példa méret megadására
    var node2 = { id: 2, parents: [], data: {virtual: true} }; // virtuálisnak jelölés (nem veszi figyelembe a pozíció számító)
    var node3 = { id: 3, parents: [node1, node2], data: {} }; // ha nincs megadva méret, akkor az alapértelmezettet fogja használni
    var node4 = { id: 4, parents: [node3], data: {name: 'Best Node Ever'} }; // bármilyen meta-adat hozzákapcsolható a csomópontokhoz
    var node5 = { id: 5, parents: [], data: {} };
    var node6 = { id: 6, parents: [node4, node5], data: {} };
    var node7 = { id: 7, parents: [], data: {} };
    var node8 = { id: 8, parents: [node7], data: {} };

    // az erdőt a fák gyökereinek tömbje alkotja
    var forest = [node6, node8];
    return forest;
}
