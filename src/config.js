/** @license MIT License (c) copyright 2016 */
/** @author Fejes Bence */

export default {
    /**
     * Spark REST API URL (for now, its just the mock JSON file).
     */
    SPARK_REST_URL: 'spark.json',

    /**
     * Describes how often the browser should poll the
     * server for new data (in ms). Also used in the ReaderMock.
     */
    SPARK_POLL_INTERVAL: 2000,

    /**
     * The minimal vertical gap between two stages (in px).
     */
    VERTICAL_GAP: 15,

    /**
     * The minimal vertical gap between two stages (in px).
     */
    HORIZONTAL_GAP: 100,

    /**
     * The default height of a node (in px).
     */
    DEFAULT_NODE_HEIGHT: 60,

    /**
     * The default width of a node (in px).
     */
    DEFAULT_NODE_WIDTH: 100,

    /**
     * The style of the connections between nodes.
     * Possible values are: "curve", "line"
     */
    CONNECTION_STYLE: 'curve',

    /**
     * Minimum zoom scale of the canvas.
     */
    ZOOM_MIN_LEVEL: 0.5,

    /**
     * Maximum zoom scale of the canvas.
     */
    ZOOM_MAX_LEVEL: 2
}
