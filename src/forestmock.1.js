import * as utils from './utils';

const json = [
  { "id": 1, "parents": [], "data": { "width": 40, "height": 40 } },
  { "id": 2, "parents": [] },
  { "id": 3, "parents": [1, 2], "data": { "width": 50 } },
  { "id": 4, "parents": [3], "data": { "width": 200, "height": 150 } },
  { "id": 5, "parents": [2], "data": { "width": 50 } },
  { "id": 6, "parents": [5, 3] },
  { "id": 7, "parents": [4, 6, 1] },
  { "id": 8, "parents": [7], "data": { "width": 50 } },
  { "id": 9, "parents": [8, 7] },
  { "id": 10, "parents": [] },
  { "id": 11, "parents": [10], "data": { "width": 250, "height": 400 } },
  { "id": 12, "parents": [11, 1, 2, 3] },
  { "id": 13, "parents": [8, 12] },
  { "id": 14, "parents": [8, 12], "data": { "width": 40 } },
  { "id": 15, "parents": [14] },
  { "id": 16, "parents": [10, 15], "data": { "width": 70 } },
  { "id": 17, "parents": [9, 16] },
  { "id": 18, "parents": [13, 17], "data": { "width": 80 } },
  { "id": 19, "parents": [18] }
];

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
if (!Array.prototype.includes) {
  Array.prototype.includes = function(searchElement /*, fromIndex*/ ) {
    'use strict';
    var O = Object(this);
    var len = parseInt(O.length) || 0;
    if (len === 0) {
      return false;
    }
    var n = parseInt(arguments[1]) || 0;
    var k;
    if (n >= 0) {
      k = n;
    } else {
      k = len + n;
      if (k < 0) {k = 0;}
    }
    var currentElement;
    while (k < len) {
      currentElement = O[k];
      if (searchElement === currentElement ||
         (searchElement !== searchElement && currentElement !== currentElement)) { // NaN !== NaN
        return true;
      }
      k++;
    }
    return false;
  };
}

/**
 * Creates a Direct Acyclic Graph (DAG) from the stage data, and
 * flags stage parents as virtual, if they were used before.
 *
 * @param stageGraphData
 */
function buildStageVDAG(stageGraphData) {
  return stageGraphData.reduce(function (_ref, stageData) {
    var rootStages = _ref.rootStages;
    var stageRepository = _ref.stageRepository;

    var _calculateStageParent = calculateStageParentsFromIds(stageData, rootStages, stageRepository);

    var parents = _calculateStageParent.parents;
    var removeFromRoots = _calculateStageParent.removeFromRoots;

    var stageVDAGNode = { id: stageData.id, parents: parents, data: stageData.extra || {} };

    return {
      rootStages: rootStages.filter(function (rootStage) {
        return !removeFromRoots.includes(rootStage);
      }).concat(stageVDAGNode),

      stageRepository: _extends({}, stageRepository, _defineProperty({}, stageVDAGNode.id, stageVDAGNode))
    };
  }, {
      rootStages: [],
      stageRepository: {}
    }).rootStages;
}

/**
 * Returns the parents of a stage, along with an array
 * of stages to remove from the root stages array.
 *
 * @param stageData
 * @param rootStages
 * @param stageRepository
 */
function calculateStageParentsFromIds(stageData, rootStages, stageRepository) {
  return stageData.parents.reduce(function (_ref2, parentStageId) {
    var parents = _ref2.parents;
    var removeFromRoots = _ref2.removeFromRoots;

    var parentStage = stageRepository[parentStageId];

    if (rootStages.includes(parentStage)) {
      return {
        parents: parents.concat({ node: parentStage, virtual: false }),
        removeFromRoots: removeFromRoots.concat(parentStage)
      };
    }

    return {
      parents: parents.concat({ node: parentStage, virtual: true }),
      removeFromRoots: removeFromRoots
    };
  }, {
      parents: [],
      removeFromRoots: []
    });
}

const vdag = buildStageVDAG(json);
const forest = utils.convertVDAGToForest(vdag);

export default function getForest() {
  return forest;
};
